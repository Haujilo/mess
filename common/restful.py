#! /usr/bin/env python
# -*- coding: utf-8 -*-

from flask import make_response, request
from flask.views import MethodView
from bson import json_util

from core.db import mongo


def _make_response(obj,
                   support_mimetypes=['application/json', 'application/xml']):
    best = request.accept_mimetypes.best
    mimetypes = best if best in support_mimetypes else 'application/json'

    if mimetypes == 'application/xml':
        # TODO: xml的支持暂时没有
        resp = make_response(None)
    else:
        resp = make_response(json_util.dumps(obj))

    resp.headers['content-type'] = '%s; charset=utf-8' % mimetypes
    return resp


def _wrapper(f):
    def __wrapper(*args, **kwargs):
        return _make_response(f(*args, **kwargs))
    return __wrapper


class MongoDBAPI(MethodView):
    decorators = [_wrapper, ]
    mongo = mongo
    cname = None  # cname为集合的名字，需要在继承后显式

    def _get_collection(self):
        return self.mongo.db[self.cname]

    def get(self, _id):
        if not _id:
            objs = self._get_collection().find()
            return objs
        else:
            obj = self._get_collection().find_one(_id)
            return obj

    def post(self):
        obj = request.form.to_dict()
        _id = self._get_collection().insert(obj)
        obj.update({'_id': _id})
        return obj

    def put(self, _id):
        obj = request.form.to_dict()
        self._get_collection().update(
            {'_id': _id},
            {'$set': obj}
        )
        obj.update({'_id': _id})
        return obj

    def delete(self, _id):
        self._get_collection().remove(_id)
        return {}


def register_api(app, view, endpoint, url, pk='_id', pk_type='string'):
    '''flask app register the MethodView'''

    view_func = view.as_view(endpoint)
    app.add_url_rule(url, defaults={pk: None},
                     view_func=view_func, methods=['GET', ])
    app.add_url_rule(url, view_func=view_func, methods=['POST', ])
    app.add_url_rule(u'%s<%s:%s>' % (url, pk_type, pk),
                     view_func=view_func, methods=['GET', 'PUT', 'DELETE'])
