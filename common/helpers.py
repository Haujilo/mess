#! /usr/bin/env python
# -*- coding: utf-8 -*-

# import base64
from bson.objectid import ObjectId
from bson.errors import InvalidId
from werkzeug.routing import BaseConverter, ValidationError


class ObjectIDConverter(BaseConverter):
    '''converter the bson model objectid'''

    def to_python(self, value):
        try:
            # return ObjectId(base64.b64decode(value))
            return ObjectId(value)
        except (InvalidId, ValueError, TypeError):
            raise ValidationError()

    def to_url(self, value):
        # return base64.b64encode(value.binary)
        return unicode(value)
