#! /usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Blueprint

from common.restful import MongoDBAPI, register_api


app = Blueprint('message', __name__)


class MessageAPI(MongoDBAPI):
    cname = 'messages'

register_api(app, MessageAPI, 'message_api', '/', '_id', 'oid')
