#! /usr/bin/env python
# -*- coding: utf-8 -*-

import re
import os

from fabric.api import env, run, local


def hosts():
    env.hosts = ['localhost', ]


def less(less_paths=['core/static/less'], css_paths=['core/static/css']):

    r = re.compile(r'(.*)\.less$')

    for less_path, css_path in zip(less_paths, css_paths):
        if not os.path.exists(css_path):
            os.mkdir(css_path)
        paths = (
            (os.path.join(less_path, path),
             os.path.join(css_path, r.sub(r'\1.css', path)))
            for path in os.listdir(less_path)
            if path.endswith('.less')
        )
        for less_file, css_file in paths:
            local('lessc %s > %s' % (less_file, css_file))
