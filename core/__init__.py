#! /usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask

import config
from db import init_db
from common.helpers import ObjectIDConverter


def create_app():

    app = Flask(__name__)
    app.config.from_object(config)
    app.url_map.converters['oid'] = ObjectIDConverter

    init_db(app)

    from core.views import app as core_app
    app.register_blueprint(core_app)
    from message.views import app as message_app
    app.register_blueprint(message_app, url_prefix='/messages')

    return app
