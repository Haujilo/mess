#! /usr/bin/env python
# -*- coding: utf-8 -*-

from flask.ext.pymongo import PyMongo

mongo = None


def init_db(app):

    global mongo
    mongo = PyMongo(app)
    return mongo
