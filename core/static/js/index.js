function getHtml(data) {
    var html = "<div><div><button class='del_bt'>x</button>" +
        "</div><div class='msg_content'>" +
        "<textarea name='content' id='{{ _id }}' disabled='disabled'>{{ content }}" +
        "</textarea></div></div>"
    html = html.replace(/{{ _id }}/, data['_id']['$oid']);
    html = html.replace(/{{ content }}/, data['content']);
    return html;
}

function show(url, k) {
    var node = $(k);

    $.get(url, function(data, s, xhr) {
        for (i in data) {
            node.append(getHtml(data[i]));
        }
    });
}

function postMsg(url, k) {
    function _postMsg() {
        var form = $(this);
        var node = $(k);

        $.post(url, form.serializeArray(), function(data, s, xhr) {
            node.append(getHtml(data));
            form.find('textarea').val('');
        });

        return false;
    }
    return _postMsg;
}

function delMsg(url) {
    return function() {
        var bt = $(this);
        var node = bt.parent('div').parent('div');
        var _id = node.find('textarea').attr('id');
        bt.attr('disabled', true);
        $.ajax({
            url: url + _id,
            type: 'DELETE',
            success: function(data, s, xhr){
                node.remove();
            },
            error:function(xhr){
                bt.attr('disabled', false);
            }
        });
    }
}

function updateMsg(url) {
    return function() {
        var node = $(this);
        var _id = node.attr('id');
        var v = node.val();
        var ori_v = node.attr('data-ori');

        if (v == ori_v){
            node.attr('disabled', true);
            node.removeAttr('data-ori');
            return false;
        }

        var data = {};
        data[node.attr('name')] = v;
        $.ajax({
            url: url + _id,
            type: 'PUT',
            data: data,
            success: function(data, s, xhr){
                node.attr('disabled', true);
                node.removeAttr('data-ori');
            },
            error:function(xhr){
                node.val(node.attr('data-ori'));
            }
        });
    }
}
