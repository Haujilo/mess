#! /usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Blueprint, render_template


app = Blueprint('core', __name__)


@app.route('/')
def index():
    return render_template('index.html')
