#! /usr/bin/env python
# -*- coding: utf-8 -*-

# DEBUG = True
# SERVER_NAME = '0.0.0.0:5000'
SECRET_KEY = 'develop key'

MONGO_DBNAME = 'test'

CACHE_REDIS_DB = 'test'
CACHE_TYPE = 'redis'
CACHE_KEY_PREFIX = 'todo_'
CACHE_REDIS_HOST = 'localhost'
CACHE_REDIS_PORT = 6379
CACHE_DEFAULT_TIMEOUT = 10000
